import { Graph } from './graph.js'
import { Vertex } from '../game/vertex.js';

export class GraphView
{
  /**
   * The graph to be rendered
   */
  public graph: Graph;

  /**
   * 
   */
  constructor()
  {
    this.graph = new Graph();
  }

  /**
   * @param pos position at which to search
   * @return Id of vertex at that position or -1 if none
   */
  public getVertexAtPosition(pos: number[]): number
  {
    for (let vertex of this.graph.vertexes)
    {
      if (pos[0] > vertex.position[0] - vertex.radius && pos[0] < vertex.position[0] + vertex.radius)
      {
        if (pos[1] > vertex.position[1] - vertex.radius && pos[1] < vertex.position[1] + vertex.radius)
        {
          return vertex.id;
        }
      }
    }

    return -1;
  }

  /**
   * Searches for a vertex by its id
   * @param id Id of the vertex to search for
   * @return The found vertex
   */
  public getVertexById(id: number): (null | Vertex)
  {
    for (let vertex of this.graph.vertexes)
    {
      if (vertex.id === id)
      {
        return vertex;
      }
    }

    return null;
  }
}