import { Vertex } from "../game/vertex.js";

export class Graph
{
  /**
   * Array of vertexes.
   * Each vertex consists of a unique identifier and some user defined data.
   */
  public vertexes: Vertex[] = [];

  /**
   * Array of edges.
   * Each edge consists of two vertex ids.
   */
  public edges: number[][] = [];

  /**
   * @param vertex Vertex or vertex id
   * @return Ids of neighboring vertexes
   */
  public neighbors(vertex:(number|Vertex)):number[]
  {
    if (vertex instanceof Vertex)
    {
      vertex = vertex.id;
    }

    let result:number[] = [];

    for (let edge of this.edges)
    {
      let neighbor:(null|number) = null;

      if (edge[0] === vertex)
      {
        neighbor = edge[1]
      }
      else if (edge[1] === vertex)
      {
        neighbor = edge[0];
      }

      if (neighbor !== null)
      {
        if (result.indexOf(neighbor) === -1)
        {
          result.push(neighbor);
        }
      }
    }

    return result;
  }
}