export class Color
{
  /**
   * @param rgba RGBA color
   * @return @rgba in css format
   */
  public static numberToString(rgba: number):string
  {
    let str = rgba.toString(16);

    str = `#${(str.length === 8) ? '' : '0'.repeat(8 - str.length)}${str}`;

    return str;
  }
}