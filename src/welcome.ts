/**
 * Welcome message and game explanation
 */
export const WELCOME:string =
`
The game is based on the <a href=https://en.wikipedia.org/wiki/Facility_location_problem>Facility Location Problem</a>.
Your goal is to gain more points than your opponent. Selecting a node will add its weight and
the weights of its neighbours to your score. The opponents select the nodes in different ways:
- Random: chooses nodes randomly.
- Highest Weight: chooses the node with the highest weight.
- Greedy: sums weight of each node including neightbours to achieve the best score.
- MinMaxer: calculates the next four moves in advance to select the node.

You can choose between two game modes:
- Levels: You play against different opponents from level one to four. Each level has a different layout.
- Custom: You can customize your own game scenario. You have the choice between four layouts, the different opponents,
the number of nodes and if some of the weights could be negative or not.

This game was created at the <a href=https://www.uni-marburg.de/fb12/arbeitsgruppen/algorithmik/index_html>Algorithmics group</a> of Philipps-Universität Marburg
as part of an internship by Lukas Schwichtenberg, Jonas Zarges and Lucas Vater. 
`;