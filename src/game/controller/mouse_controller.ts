import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { Main } from '../../index.js';

export class MouseController
{
  /**
   * Event-receiving canvas
   */
  private _canvas: HTMLCanvasElement;

  /**
   * Node that the mouse hovered over the last time {@link onMouseMove} was
   * called.
   */
  private _oldHoverNode: number = -1;


  /**
   * Translates event- to local coordinated
   * @param coordinates Window coordinates [x, y]
   * @return Local coordinates [x, y]
   */
  private _translateCoordinates(coordinates: (MouseEvent | number[])): number[]
  {
    if (coordinates instanceof MouseEvent)
    {
      coordinates = [coordinates.clientX, coordinates.clientY];
    }

    let rect = this._canvas.getBoundingClientRect();

    return [
      (coordinates[0] - rect.left) / rect.width,
      (coordinates[1] - rect.top) / rect.height
    ];
  }

  /**
   * Click event handler
   * @param event mouse event
   */
  public onClick(ev: MouseEvent): void
  {
    let position = this._translateCoordinates(ev);
    let vertexId = Main.graphView.getVertexAtPosition(position);

    if (vertexId !== -1)
    {
      let vertex: Vertex = Main.graphView.getVertexById(vertexId)!;

      if (Main.currentPlayer)
      {
        Main.currentPlayer.onVertexClicked(vertex);
      }
    }
  }

  /**
   * Mouse move event handler
   * @param ev Mouse event
   */
  public onMouseMove(ev: MouseEvent)
  {
    const VERTEX_HOVER_GROW = 1.1;

    let position = this._translateCoordinates(ev);

    let vertexId = Main.graphView.getVertexAtPosition(position);

    if (this._oldHoverNode !== -1)
    {
      if (vertexId !== this._oldHoverNode)
      {
        let vertex: (null | Vertex) = Main.graphView.getVertexById(this._oldHoverNode);

        if (vertex)
        {
          // {@link vertex} can be null when the level changed
          vertex.radius /= VERTEX_HOVER_GROW;
        }
        
        this._oldHoverNode = -1;
      }
    }

    if (vertexId !== -1)
    {
      let vertex = Main.graphView.getVertexById(vertexId)!;

      if (vertex.id !== this._oldHoverNode)
      {
        if (!vertex.owner)
        {
          vertex.radius *= VERTEX_HOVER_GROW;
          this._oldHoverNode = vertex.id;
        }
      }
    }
  }

  /**
   * @param canvas Event-receiving canvas
   * @param graphView The graph view displayed in @canvas
   */
  constructor(canvas: HTMLCanvasElement)
  {
    this._canvas = canvas;
  }
}