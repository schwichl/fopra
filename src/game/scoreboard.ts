import { Main } from "../index.js";
import { Player } from "./player/player.js";
import { Color } from "../color.js";

export class ScoreBoard
{
  /**
   * 
   */
  private static addEntry(player:Player):void
  {
    let scoreboard = <HTMLDivElement> document.querySelector('#scoreboard');
    let template = <HTMLTemplateElement> scoreboard.querySelector('#t-entry');

    let entry = <HTMLDListElement> document.importNode(template.content, true).querySelector('.entry')!;

    entry.style.backgroundColor = Color.numberToString(player.color);

    (<HTMLDataElement> entry.querySelector('.name')).innerText = player.name;
    (<HTMLDataElement> entry.querySelector('.score')).innerText = player.score.toString();

    if (player === Main.currentPlayer)
    {
      entry.classList.add('active');
    }

    if (player.winner)
    {
      entry.classList.add('winner');
    }

    scoreboard.appendChild(entry);
  }

  /**
   * Updates the scoreboard
   */
  public static update(players:Player[]):void
  {
    let scoreboard = <HTMLDivElement> document.querySelector('#scoreboard');

    while (scoreboard.children.length > 1)
    {
      // Don't remove children[0], because that's the template
      scoreboard.removeChild(scoreboard.children[1]);
    }

    for (let player of players)
    {
      ScoreBoard.addEntry(player);
    }
  }
}