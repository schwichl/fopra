import { GraphView } from '../../graph/graph_view.js';

export abstract class WeightDistributor
{
  /**
   * Distributing Weight
   */ 
  abstract distribute(graph_view:GraphView):void;
}