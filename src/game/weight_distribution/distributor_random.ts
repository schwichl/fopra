import { WeightDistributor } from "./weight_distributor.js"
import { GraphView } from "../../graph/graph_view.js"
import { Vertex } from "../vertex.js";

export class RandomDistributor extends WeightDistributor
{

  private range: [number, number];

  constructor(range: [number, number])
  {
    super()
    this.range = range;
  }

  distribute(graph_view: GraphView): void
  {
    for (let v of graph_view.graph.vertexes)
    {
      //Set weight to random number in range
      v.weight = Math.floor(Math.random() * (this.range[1] - this.range[0] + 1) + this.range[0])
    }
  }

}
