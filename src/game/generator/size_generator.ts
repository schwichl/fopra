export abstract class SizeGenerator
{
  protected _current:number;

  /**
   * @return Minimum size
   */
  public abstract min(): number;

  /**
   * @return Maximum size
   */
  public abstract max(): number;

  /**
   * @return Next size
   */
  public next(): (null|number)
  {
    if (this._current > this.max())
    {
      console.error(`size generator exceeded maximum value (${this._current} > ${this.max()})`);
      return null;
    }

    return this._current;
  }

  /**
   * @return Amount of possible sizes
   */
  public abstract length(): number;

  /**
   * Resets the generator to it's minimum size.
   */
  public reset(): void
  {
    this._current = this.min();
  }

  /**
   * 
   */
  constructor()
  {
    this._current = this.min();
  }
}