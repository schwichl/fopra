import { GraphGenerator } from './graph_generator.js';
import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { SizeGenerator } from './size_generator.js';

//Import Delaunay triangulation library
declare var Delaunay: any;

class _SizeGenerator extends SizeGenerator
{
  private _base: number = 1;

  /**
   * @override
   */
  public min(): number
  {
    return 1;
  }

  /**
   * @override
   */
  public max(): number
  {
    return 100;
  }

  /**
   * @override
   */
  public length(): number
  {
    return 100;
  }

  /**
   * @override
   */
  public next(): (null|number)
  {
    let next = super.next();

    ++this._current;

    return next;
  }
};

export class GeneratorRTree extends GraphGenerator<Vertex>
{
  /**
   * Generates random vertices by creating a grid and
   * choosing random spots on it
   * @param result
   * @param size vertex count
   */
  private generateVerticesGrid(result: GraphView, size: number)
  {
    let squareSize = Math.ceil(Math.sqrt(size) * 2);
    let spacing = 1 / squareSize * 0.1;
    let nodeRadius = (1 / squareSize) * 0.5;
    let offset = nodeRadius

    //Generate a grid of squareSize size
    let possibleVertices: [number, number][] = [];
    for (let x = 0; x < squareSize; ++x)
    {
      for (let y = 0; y < squareSize; ++y)
      {
        possibleVertices.push([x, y]);
      }
    }

    //Choose random spots in the generated grid and create a vertex there
    let id = 0;
    for (let i = 0; i < size; i++)
    {
      let v = possibleVertices.splice(Math.floor(Math.random() * possibleVertices.length), 1)[0];
      let vertex = new Vertex();
      vertex.radius = nodeRadius - spacing;
      vertex.position = [
        offset + v[0] * (nodeRadius * 2),
        offset + v[1] * (nodeRadius * 2)
      ]
      vertex.weight = 1;
      vertex.id = id++;
      result.graph.vertexes.push(vertex);
    }
  }

  /**
   * Returns an edge with minimal weight
   * @param edges an array of weighted edges
   */
  private getMinEdge(edges: [number, number, number][]): number
  {
    let min = edges[0][2];
    let minIndex = 0;
    for (let i = 0; i < edges.length; i++)
    {
      if (edges[i][2] < min)
      {
        min = edges[i][2];
        minIndex = i;
      }
    }
    return minIndex;
  }

  /**
   * Helper function to perform a DFS to look for cycles in a graph
   * @param edges an array of edges
   * @param v vertex
   * @param visited array of visited vertices
   * @param parent last vertex
   */
  private isCyclicRecursive(edges: [number, number, number][], v: number, visited: boolean[], parent: number): boolean
  {
    //Mark current vertex as visited
    visited[v] = true;

    //Search for adjacent vertices
    for (let edge of edges)
    {
      let adjacent = undefined;
      if (edge[0] === v)
      {
        adjacent = edge[1];
      } else if (edge[1] === v)
      {
        adjacent = edge[0];
      }
      if (adjacent != undefined)
      {
        //adjacent vertex has not already been visited
        if (!visited[adjacent])
        {
          if (this.isCyclicRecursive(edges, adjacent, visited, v))
          {
            return true;
          }
        }
        else if (adjacent != parent) //Vertex has been visited already and is not the parent
        {
          return true; //Cycle found
        }
      }
    }
    return false;
  }

  /**
   * Returns if a given graph is cyclic
   * @param vertices array of vertices
   * @param edges array of edges
   */
  private isCyclic(vertices: number[][], edges: [number, number, number][])
  {
    let visited: boolean[] = [];
    for (let i = 0; i < vertices.length; i++)
    {
      visited.push(false);
    }

    //Loop through each vertex, looking if there is a cycle when performing
    //a DFS starting from there
    for (let i = 0; i < vertices.length; i++)
    {
      if (!visited[i])
      {
        if (this.isCyclicRecursive(edges, i, visited, -1))
        {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Returns a minimal spanning tree using the Kruskal algorithm
   * https://de.wikipedia.org/wiki/Algorithmus_von_Kruskal
   * 
   * @param vertices array of vertices
   * @param edges array of weighted edges
   */
  private kruskal(vertices: number[][], edges: [number, number, number][]): [number, number, number][]
  {
    let spanning: [number, number, number][] = [];
    while (edges.length > 0)
    {
      let minEdge = this.getMinEdge(edges); //Get minimal edge
      let curEdge = edges.splice(minEdge, 1)[0]; //Remove edge

      spanning.push(curEdge);

      //Remove edge, if it introduces a cycle
      if (this.isCyclic(vertices, spanning))
      {
        spanning.pop();
      }

    }

    return spanning;
  }


  /**
   * Do two given lines intersect? 
   */
  private linesIntersects(p1: number[], p2: number[], p3: number[], p4: number[]): boolean
  {
    let det = (p2[0] - p1[0]) * (p4[1] - p3[1]) - (p4[0] - p3[0]) * (p2[1] - p1[1]);
    if (det === 0)
    {
      return false;
    } else
    {
      let lambda = ((p4[1] - p3[1]) * (p4[0] - p1[0]) + (p3[0] - p4[0]) * (p4[1] - p1[1])) / det;
      let gamma = ((p1[1] - p2[1]) * (p4[0] - p1[0]) + (p2[0] - p1[0]) * (p4[1] - p1[1])) / det;
      return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
  };

  /**
   * Is a line intersecting the bounding box of a circle?
   */
  private lineIntersectsCircle(p1: number[], p2: number[], c: number[], r: number)
  {
    //Calculate bounding box
    let r1: number[] = [c[0] - r, c[1] - r];
    let r2: number[] = [c[0] + r, c[1] - r];
    let r3: number[] = [c[0] + r, c[1] + r];
    let r4: number[] = [c[0] - r, c[1] + r];

    //Check line intersection with each edge of bounding box
    let side1 = this.linesIntersects(p1, p2, r1, r2);
    let side2 = this.linesIntersects(p1, p2, r2, r3);
    let side3 = this.linesIntersects(p1, p2, r4, r4);
    let side4 = this.linesIntersects(p1, p2, r4, r1);

    return (side1 || side2 || side3 || side4);

  }

  generate(size: number): GraphView
  {

    let overlap = true;
    let result = undefined;
    while (overlap)
    {
      result = new GraphView();

      //Generate random vertices
      this.generateVerticesGrid(result, size);


      let vertices: number[][] = [];
      for (let v of result.graph.vertexes)
      {
        vertices.push(v.position);
      }

      //Triangulate vertices to form a graph with minimal crossings using Delaunay-Triangulation
      //https://de.wikipedia.org/wiki/Delaunay-Triangulierung
      //Uses: https://github.com/ironwallaby/delaunay Library
      let triangulated = Delaunay.triangulate(vertices);

      //Create edges from triangulation with random weights assigned
      let edges: [number, number, number][] = [];
      for (let i = 0; i < triangulated.length; i += 3)
      {
        edges.push([triangulated[i], triangulated[i + 1], Math.random() * 20]);
        edges.push([triangulated[i + 1], triangulated[i + 2], Math.random() * 20]);
        edges.push([triangulated[i + 2], triangulated[i], Math.random() * 20]);
      }

      //Create a minimal spanning tree to remove cycles
      let spanTree = this.kruskal(vertices, edges);

      //Add edges to result
      for (let e of spanTree)
      {
        result.graph.edges.push([e[0], e[1]]);
      }

      //Check overlap
      overlap = false;
      for (let e of result.graph.edges)
      {
        let v1: Vertex = result.getVertexById(e[0])!;
        let v2: Vertex = result.getVertexById(e[1])!;
        for (let vert of result.graph.vertexes)
        {
          if (vert == v1 || vert == v2) continue;
          if (this.lineIntersectsCircle(v1.position, v2.position, vert.position, vert.radius))
          {
            overlap = true;
            break;
          }
        }
      }
    }

    return result!;
  }

  /**
   * 
   */
  constructor()
  {
    super(new _SizeGenerator());
  }
}