import { GraphView } from '../../graph/graph_view.js';
import { SizeGenerator } from './size_generator.js';

export abstract class GraphGenerator<T>
{
  /**
   * 
   */
  private _sizeGenerator: SizeGenerator;

  /**
   * 
   */
  public getSizeGenerator(): SizeGenerator
  {
    return this._sizeGenerator;
  }

  /**
   * 
   */
  abstract generate(size: number): GraphView;

  /**
   * 
   */
  constructor(sizeGenerator: SizeGenerator)
  {
    this._sizeGenerator = sizeGenerator;
  }
}