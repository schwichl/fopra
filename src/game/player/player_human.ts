import { Player } from './player.js';
import { Vertex } from '../vertex.js';

export class PlayerHuman extends Player
{
  /**
   * @override
   */
  public onVertexClicked(vertex: Vertex): void
  {
    if (!vertex.owner)
    {
      this.capture(vertex);

      this._onMoveComplete();
    }
  }

  /**
   * @override
   */
  constructor(onMoveComplete?: () => void)
  {
    super(onMoveComplete);

    this.name = 'Player1';
    this.color = 0x5050E0FF;
    this.colorLight = 0xA0A0FFFF;
  }
}