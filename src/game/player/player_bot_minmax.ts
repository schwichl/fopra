import { PlayerBot } from './player_bot.js';
import { Player } from './player.js';
import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { Main } from '../../index.js';


interface GameTurn
{
  player: number;
  vertices: number[];
}

/**
 * Wrapper for the current game state
 */
class GameState
{
  private taken: Map<number, number> = new Map<number, number>();
  private graph: GraphView;

  //Evaluates the current state of a player
  public evaluate(player: number): number
  {
    let sumOwn = 0;
    let sumOpponent = 0;

    for (let vid of this.taken.keys())
    {
      if (this.taken.get(vid) === player)
      {
        sumOwn += this.graph.getVertexById(vid)!.weight;
      }
      else if (this.taken.get(vid) === -player)
      {
        sumOpponent += this.graph.getVertexById(vid)!.weight;
      }
    }
    return sumOwn - sumOpponent;
  }

  public isValidTurn(vertex: number): boolean
  {
    return !this.taken.has(vertex);
  }

  public getValidTurns(): number[]
  {
    let turns: number[] = [];

    for (let v of this.graph.graph.vertexes)
    {
      if (this.isValidTurn(v.id)) turns.push(v.id);
    }

    return turns;
  }

  public makeTurn(player: number, vertex_id: number): GameTurn
  {
    let vertex = this.graph.getVertexById(vertex_id)!;
    let neighbours = this.graph.graph.neighbors(vertex);
    let taken_vertices: number[] = [];
    this.taken.set(vertex_id, player);
    taken_vertices.push(vertex_id);

    for (let neighbour of neighbours)
    {
      if (this.isValidTurn(neighbour))
      {
        this.taken.set(neighbour, player);
        taken_vertices.push(neighbour);
      }
    }

    let turn = {
      player: player,
      vertices: taken_vertices
    };

    return turn;
  }

  public reverseTurn(turn: GameTurn)
  {
    for (let vid of turn.vertices)
    {
      this.taken.delete(vid);
    }
  }

  constructor(graph: GraphView, bot: Player)
  {
    this.graph = graph;
    for (let v of graph.graph.vertexes)
    {
      if (v.owner === bot)
      {
        this.taken.set(v.id, 1);
      }
      else if (v.owner !== null)
      {
        this.taken.set(v.id, -1);
      }
    }
  }
}

export class PlayerBotMinMax extends PlayerBot
{
  private selectedTurn: number = -1;


  //Minimax-Algorithmus
  private minmax(state: GameState, player: number, depth: number, maxDepth: number): number
  {
    let validTurns = state.getValidTurns();

    if (depth === 0 || validTurns.length === 0)
    {
      return state.evaluate(player);
    }

    let maxValue = -Infinity;
    for (let turn of validTurns)
    {
      let newTurn = state.makeTurn(player, turn);
      let value = -this.minmax(state, -player, depth - 1, maxDepth);
      state.reverseTurn(newTurn);

      if (value > maxValue)
      {
        maxValue = value;

        if (depth === maxDepth)
        {
          this.selectedTurn = turn;
        }
      }
    }

    return maxValue;
  }

  //Alpha-Beta-Suche(Optimierter Minimax)
  private alphabeta(state: GameState, player: number, depth: number, alpha: number, beta: number, maxDepth: number): number
  {
    let validTurns = state.getValidTurns();

    if (depth === 0 || validTurns.length === 0)
    {
      return state.evaluate(player);
    }

    let maxValue = alpha;

    for (let turn of validTurns)
    {
      let newTurn = state.makeTurn(player, turn);
      let value = -this.alphabeta(state, -player, depth - 1, -beta, -maxValue, maxDepth);

      state.reverseTurn(newTurn);

      if (value > maxValue)
      {
        maxValue = value;
        if (maxValue >= beta)
        {
          break;
        }

        if (depth === maxDepth)
        {
          this.selectedTurn = turn;
        }
      }
    }

    return maxValue;
  }

  /**
  * @override
  */
  public move(): void
  {
    this._lastTimeoutHandle = setTimeout(() =>
    {
      this.selectedTurn = -1;
      let maxDepth = 4;
      let state = new GameState(Main.graphView, this);
      //let score = this.minmax(state, 1, maxDepth, maxDepth);
      let score = this.alphabeta(state, 1, maxDepth, -Infinity, Infinity, maxDepth);
      if (this.selectedTurn >= 0)
      {
        this.capture(Main.graphView.getVertexById(this.selectedTurn)!);
      }

      //I'm done!
      this._onMoveComplete();

      this._lastTimeoutHandle = -1;

    }, this._THINK_TIME);
  }

  /**
 * @override
 */
  constructor(onMoveComplete?: () => void)
  {
    super(onMoveComplete);

    this.name = 'MinMaxer';
    this.color = 0x30D030FF;
    this.colorLight = 0x80F080FF;
  }
}