import { PlayerBot } from './player_bot.js';
import { Vertex } from '../vertex.js';
import { Main } from '../../index.js';

export class PlayerBotRandom extends PlayerBot
{
  /**
   * @override
   */
  public move(): void
  {
    let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);

    console.assert(freeVertexes.length > 0);

    this._lastTimeoutHandle = setTimeout(() =>
    {
      this.capture(freeVertexes[Math.floor(Math.random() * freeVertexes.length)]);

      this._onMoveComplete();
      this._lastTimeoutHandle = -1;
    }, this._THINK_TIME);
  }

  /**
   * @override
   */
  constructor(onMoveComplete?: () => void)
  {
    super(onMoveComplete);

    this.name = 'Randy';
    this.color = 0xE04050FF;
    this.colorLight = 0xFFA0A0FF;
  }
}