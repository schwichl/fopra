import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { Main } from '../../index.js';

export abstract class Player
{
  /**
   * Display name
   */
  public name: string = '';

  /**
   * Color used to mark captured nodes
   */
  public color: number = 0x00000000;

  /**
   * Color used to mark adjacent nodes
   */
  public colorLight: number = 0x00000000;

  /**
   * True if the game is over and this player has won
   */
  public winner: boolean = false;

  /**
   * Sum of all vertexes taken by this player
   */
  public score: number = 0;

  /**
   * onMoveComplete callback
   */
  protected _onMoveCompleteImpl?: () => void;

  /**
   * Timeout handle for bots
   */
  protected _lastTimeoutHandle = -1;

  /**
   * Called by child classes
   */
  protected _onMoveComplete(): void
  {
    console.assert(!!this._onMoveCompleteImpl);

    this._onMoveCompleteImpl!();
  }

  /**
   * Captures a single vertex, ignoring it's neighbors.
   * The call is ignore if @vertex is already taken
   * @param vertex The vertex to capture
   */
  private _captureImpl(vertex: Vertex, light: boolean = false): void
  {
    if (vertex.owner !== null)
    {
      return;
    }

    vertex.owner = this;

    if (light)
    {
      vertex.color = this.colorLight;
    }
    else
    {
      vertex.color = this.color;
    }

    this.score += vertex.weight;
  }

  /**
   * Installs or overrides an onMoveComplete callback
   */
  public setOnMoveComplete(onMoveComplete:() => void)
  {
    this._onMoveCompleteImpl = onMoveComplete;
  }

  /**
   * Consumes a vertex and all of it's free neighbors.
   * Increases the player's score.
   * @param vertex The vertex to be captured
   */
  public capture(vertex: Vertex): void
  {
    this._captureImpl(vertex);

    for (let neighbor of Main.graphView.graph.neighbors(vertex))
    {
      this._captureImpl(Main.graphView.getVertexById(neighbor)!, true);
    }
  }

  /**
   * Resets score and winner state
   */
  public resetScore(): void
  {
    this.score = 0;
    this.winner = false;
  }

  /**
   * Clears the timeout
   */
  public clearTimeouts(): void
  {
    if(this._lastTimeoutHandle > -1)
    {
      clearTimeout(this._lastTimeoutHandle);
    }
  }

  /**
   * Fired when the previous player finished their move
   */
  public move(): void
  { }

  /**
   * Gets called when a vertex is clicked and it's this player's turn
   * @param vertex The clicked vertex
   */
  public onVertexClicked(vertex: Vertex): void
  { }

  /**
   * 
   */
  constructor(onMoveComplete?: () => void)
  {
    this._onMoveCompleteImpl = onMoveComplete;
  }
}