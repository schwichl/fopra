import { Player } from './player.js';

export class PlayerBot extends Player
{
  /**
   * Artificial think time
   */
  protected readonly _THINK_TIME:number = 1000;
}