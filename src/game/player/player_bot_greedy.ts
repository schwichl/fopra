import { PlayerBot } from './player_bot.js';
import { Vertex } from '../vertex.js';
import { Main } from '../../index.js';

export class PlayerBotGreedy extends PlayerBot
{
  /**
   * @return Sum of the weights of the given vertex and all of its free
   *         neighbors.
   */
  private _getFullWeight(vertex: Vertex)
  {
    let result: number = vertex.weight;

    for (let id of Main.graphView.graph.neighbors(vertex))
    {
      let neighbor: Vertex = Main.graphView.getVertexById(id)!;

      if (!neighbor.owner)
      {
        result += neighbor.weight;
      }
    }

    return result;
  }

  /**
   * @override
   */
  public move(): void
  {
    let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);

    console.assert(freeVertexes.length > 0);

    this._lastTimeoutHandle = setTimeout(() =>
    {
      let bestVertex: Vertex = freeVertexes[0];
      let bestScore: number = this._getFullWeight(freeVertexes[0]);

      for(let i = 1; i < freeVertexes.length; i++)
      {
        let fullWeight = this._getFullWeight(freeVertexes[i]);

        if (fullWeight > bestScore)
        {
          bestScore = fullWeight;
          bestVertex = freeVertexes[i];
        }
      }
      /*
      for (let vertex of freeVertexes)
      {
        let fullWeight = this._getFullWeight(vertex);

        if (fullWeight > bestScore)
        {
          bestScore = fullWeight;
          bestVertex = vertex;
        }
      }
      */
      this.capture(bestVertex!);

      this._onMoveComplete();
      this._lastTimeoutHandle = -1;
    }, this._THINK_TIME);
  }

  /**
   * @override
   */
  constructor(onMoveComplete?: () => void)
  {
    super(onMoveComplete);

    this.name = 'Gree';
    this.color = 0x30D030FF;
    this.colorLight = 0x80F080FF;
  }
}