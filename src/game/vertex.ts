import { Player } from './player/player.js';

export class Vertex
{
  /**
   * Unique id
   */
  public id: number = 0;

  /**
   * x, y
   */
  public position: number[] = [0, 0];

  /**
   * Radius
   */
  public radius: number = 0.0;

  /**
   * Color
   */
  public color: number = 0x000000FF;

  /**
   * Weight
   */
  public weight: number = 0;

  /**
   * Owner of the vertex.
   */
  private _owner: (null | Player) = null;

  /**
   * 
   */
  public get owner():(null | Player)
  {
    return this._owner;
  }

  /**
   * 
   */
  public set owner(owner:(null | Player))
  {
    this._owner = owner;
  }
}