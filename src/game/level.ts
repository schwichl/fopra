import { Player } from './player/player.js';
import { GraphGenerator } from './generator/graph_generator.js';
import { Vertex } from './vertex.js';

export class Level
{
  /**
   * The opponent
   */
  public opponent: Player;

  /**
   * The graph generator
   */
  public generator: GraphGenerator<Vertex>;

  /**
   * Size used to generate the graph
   */
  public size: number = 0;

  /**
   * 
   */
  constructor(opponent: Player, generator: GraphGenerator<Vertex>)
  {
    this.opponent = opponent;
    this.generator = generator;
  }
}