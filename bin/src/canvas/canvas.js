import { Color } from "../color.js";
export class Canvas {
    /**
     * Generates a linear gradient
     */
    _linearGradient(vertexes, colors) {
        let gradientColors = ((typeof (colors) === 'number') ? [colors] : colors);
        let gradient = this._context.createLinearGradient(vertexes[0][0], vertexes[0][1], vertexes[vertexes.length - 1][0], vertexes[vertexes.length - 1][1]);
        // Avoid division by 0 for 1-element arrays
        let maxIndex = ((gradientColors.length === 1) ? 1 : gradientColors.length - 1);
        gradientColors.forEach((color, i) => {
            gradient.addColorStop(i / maxIndex, Color.numberToString(color));
        });
        return gradient;
    }
    /**
     * Sets the context's fill or stroke style and calls fill or stroke
     * @param color RGBA color
     * @param fill Fill
     * @param lineWidth Line width in px
     */
    _fillOrStroke(color, fill, lineWidth = 1) {
        let colorString = Color.numberToString(color);
        if (fill) {
            this._context.fillStyle = colorString;
            this._context.fill();
        }
        else {
            this._context.lineWidth = lineWidth;
            this._context.strokeStyle = colorString;
            this._context.stroke();
        }
    }
    /**
     * @return Canvas width
     */
    get width() {
        return this._contextDimensions[0];
    }
    /**
     * @return Canvas height
     */
    get height() {
        return this._contextDimensions[1];
    }
    /**
     * @param text Text
     * @param font Font name
     * @param size Font size in px
     * @return Text width and height. The current implementation returns @size as
     *         height.
     */
    measureText(text, font, size) {
        this._context.font = `${size}px ${font}`;
        let measurements = this._context.measureText(text);
        return [measurements.width, size];
    }
    /**
     * Clears the scene
     */
    clear() {
        this._context.clearRect(0, 0, this._contextDimensions[0], this._contextDimensions[1]);
    }
    /**
     * Draws a line
     * @param a Start position
     * @param b End position
     * @param color Color or gradient
     * @param lineWidth Line width in px
     */
    line(a, b, color, lineWidth = 1) {
        this._context.beginPath();
        this._context.moveTo(a[0], a[1]);
        this._context.lineTo(b[0], b[1]);
        this._context.lineWidth = lineWidth;
        this._context.strokeStyle = this._linearGradient([a, b], color);
        this._context.stroke();
    }
    /**
     * Draws a circle
     * @param center Center position
     * @param radius Radius
     * @param color Color
     * @param fill Fill or stroke
     * @param lineWidth Line width in px
     */
    circle(center, radius, color, fill = false, lineWidth = 1) {
        const PI2 = 2.0 * Math.PI;
        this._context.beginPath();
        this._context.arc(center[0], center[1], radius, 0.0, PI2, false);
        this._fillOrStroke(color, fill, lineWidth);
    }
    /**
     * Draws a rectangle
     * @param position Top left position
     * @param dimensions Width and height
     * @param color Color
     * @param fill Fill or stroke
     * @param lineWidth Line width in px
     */
    rectangle(position, dimensions, color, fill = false, lineWidth = 1) {
        this._context.beginPath();
        this._context.rect(position[0], position[1], dimensions[0], dimensions[1]);
        this._fillOrStroke(color, fill, lineWidth);
    }
    /**
     * Draws text
     * @param position Top left position
     * @param text Text
     * @param font Font name
     * @param size Font size in px
     * @param fill Fill or stroke
     * @param lineWidth Line width in px
     */
    text(position, text, font, size, color, fill = false, lineWidth = 1) {
        // 2d context text element have their origin at the bottom left
        position = [position[0], position[1] + size];
        let colorString = Color.numberToString(color);
        this._context.beginPath();
        this._context.font = `${size}px ${font}`;
        if (fill) {
            this._context.fillStyle = colorString;
            this._context.fillText(text, position[0], position[1]);
        }
        else {
            this._context.strokeStyle = colorString;
            this._context.lineWidth = lineWidth;
            this._context.strokeText(text, position[0], position[1]);
        }
    }
    /**
     *
     */
    graphView(graphView) {
        const LINE_WIDTH = 2;
        const NODE_FONT_FAMILY = 'sans-serif';
        const NODE_FONT_SIZE = 32;
        for (let node of graphView.graph.vertexes) {
            let targetPosition = [
                node.position[0] * this._contextDimensions[0],
                node.position[1] * this._contextDimensions[1]
            ];
            this.circle(targetPosition, (node.radius * this._contextDimensions[0]) - (LINE_WIDTH / 2), node.color, false, LINE_WIDTH);
            let font_size = NODE_FONT_SIZE;
            if (node.radius < 0.05) {
                font_size = NODE_FONT_SIZE / 2;
            }
            let label = node.weight.toString();
            let labelDimensions = this.measureText(label, NODE_FONT_FAMILY, font_size);
            let labelPosition = [
                targetPosition[0] - labelDimensions[0] / 2.0,
                targetPosition[1] - labelDimensions[1] / 2.0
            ];
            this.text(labelPosition, label, NODE_FONT_FAMILY, font_size, node.color, true);
            // this.text(labelPosition, label, NODE_FONT_FAMILY, NODE_FONT_SIZE, 0x000000FF, false);
        }
        for (let edge of graphView.graph.edges) {
            let nodeStart = graphView.getVertexById(edge[0]);
            let nodeEnd = graphView.getVertexById(edge[1]);
            let direction = [
                (nodeEnd.position[0] - nodeStart.position[0]),
                (nodeEnd.position[1] - nodeStart.position[1])
            ];
            let length = Math.sqrt(direction[0] * direction[0] + direction[1] * direction[1]);
            direction[0] /= length;
            direction[1] /= length;
            let lineStart = [
                (nodeStart.position[0] + direction[0] * nodeStart.radius) * this._contextDimensions[0],
                (nodeStart.position[1] + direction[1] * nodeStart.radius) * this._contextDimensions[1]
            ];
            let lineEnd = [
                (nodeEnd.position[0] - direction[0] * nodeEnd.radius) * this._contextDimensions[0],
                (nodeEnd.position[1] - direction[1] * nodeEnd.radius) * this._contextDimensions[1]
            ];
            this.line(lineStart, lineEnd, (nodeStart.owner === nodeEnd.owner) ? [nodeStart.color, nodeEnd.color] : [nodeStart.color, 0x000000FF, nodeEnd.color], LINE_WIDTH);
        }
    }
    /**
     * Fits the canvas size to the window
     */
    fitToWindow() {
        this._context.canvas.width = Math.min(this._context.canvas.parentElement.clientWidth, window.innerHeight);
        this._context.canvas.height = this._context.canvas.width;
        this._contextDimensions = [this._context.canvas.width, this._context.canvas.height];
    }
    /**
     *
     */
    constructor(context) {
        this._context = context;
        this.fitToWindow();
        this._contextDimensions = [this._context.canvas.width, this._context.canvas.height];
    }
}
//# sourceMappingURL=canvas.js.map