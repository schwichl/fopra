/**
 * Welcome message and game explanation
 */
export const WELCOME = `
Your goal is to gain more points than your opponent.
Selecting a node will add its weight and the weights of its neighbors to your
score.
Press OK to start.
`;
//# sourceMappingURL=welcome.js.map