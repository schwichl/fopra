import { WeightDistributor } from "./weight_distributor.js";
export class RandomDistributor extends WeightDistributor {
    constructor(range) {
        super();
        this.range = range;
    }
    distribute(graph_view) {
        for (let v of graph_view.graph.vertexes) {
            //Set weight to random number in range
            v.weight = Math.floor(Math.random() * (this.range[1] - this.range[0] + 1) + this.range[0]);
        }
    }
}
//# sourceMappingURL=distributor_random.js.map