import { Player } from './player.js';
export class PlayerBot extends Player {
    constructor() {
        super(...arguments);
        /**
         * Artificial think time
         */
        this._THINK_TIME = 1000;
    }
}
//# sourceMappingURL=player_bot.js.map