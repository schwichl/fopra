import { Main } from "../index.js";
import { Color } from "../color.js";
export class ScoreBoard {
    /**
     *
     */
    static addEntry(player) {
        let scoreboard = document.querySelector('#scoreboard');
        let template = scoreboard.querySelector('#t-entry');
        let entry = document.importNode(template.content, true).querySelector('.entry');
        entry.style.backgroundColor = Color.numberToString(player.color);
        entry.querySelector('.name').innerText = player.name;
        entry.querySelector('.score').innerText = player.score.toString();
        if (player === Main.currentPlayer) {
            entry.classList.add('active');
        }
        if (player.winner) {
            entry.classList.add('winner');
        }
        scoreboard.appendChild(entry);
    }
    /**
     * Updates the scoreboard
     */
    static update(players) {
        let scoreboard = document.querySelector('#scoreboard');
        while (scoreboard.children.length > 1) {
            // Don't remove children[0], because that's the template
            scoreboard.removeChild(scoreboard.children[1]);
        }
        for (let player of players) {
            ScoreBoard.addEntry(player);
        }
    }
}
//# sourceMappingURL=scoreboard.js.map