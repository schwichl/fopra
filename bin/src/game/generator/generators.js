import { GeneratorLine } from "./generator_line.js";
import { GeneratorGrid } from "./generator_grid.js";
import { GeneratorTree } from "./generator_tree.js";
import { GeneratorRTree } from "./generator_rtree.js";
/**
 * @displayName is used for the debug GUI
 */
export const GENERATOR_MODES = {
    'line': {
        displayName: 'Line',
        generator: () => new GeneratorLine()
    },
    'grid': {
        displayName: 'Grid',
        generator: () => new GeneratorGrid()
    },
    'tree': {
        displayName: 'Binary Tree',
        generator: () => new GeneratorTree()
    },
    'rtree': {
        displayName: 'Random Tree',
        generator: () => new GeneratorRTree()
    }
};
//# sourceMappingURL=generators.js.map