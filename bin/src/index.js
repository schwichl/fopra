import { GeneratorLine } from './game/generator/generator_line.js';
import { GeneratorGrid } from './game/generator/generator_grid.js';
import { GeneratorTree } from './game/generator/generator_tree.js';
import { GENERATOR_MODES } from './game/generator/generators.js';
import { Canvas } from './canvas/canvas.js';
import { MouseController } from './game/controller/mouse_controller.js';
import { RandomDistributor } from './game/weight_distribution/distributor_random.js';
import { RandomNegativeDistributor } from './game/weight_distribution/distributor_negative_random.js';
import { GeneratorRTree } from './game/generator/generator_rtree.js';
import { PlayerHuman } from './game/player/player_human.js';
import { PlayerBotRandom } from './game/player/player_bot_random.js';
import { BOT_MODES } from './game/player/bots.js';
import { ScoreBoard } from './game/scoreboard.js';
import { PlayerBotGreedy } from './game/player/player_bot_greedy.js';
import { Level } from './game/level.js';
import { PlayerBotHighestWeight } from './game/player/player_bot_highest_weight.js';
import { WELCOME } from './welcome.js';
export class Main {
    /**
     *
     */
    constructor() {
        /**
         * Whether or not debugging features (eg. graph generator) should be
         * available.
         */
        this._debug = true;
        /**
         * Players
         */
        this._players = [];
        /**
         * Levels
         */
        this._levels = [];
        /**
         * Index of the current level
         */
        this._currentLevel = 0;
        /**
         * Positive and negative vertex weight
         */
        this._negativeWeight = true;
        if (this._debug) {
            window.main = this;
        }
        this._domCanvas = document.querySelector('#main-graph');
        let ctxMainGraph = this._domCanvas.getContext('2d');
        this._canvas = new Canvas(ctxMainGraph);
        this._prepareUI();
        if (this._debug) {
            this._populateConfig();
            this._installConfigCallbacks();
            this._applyDebugConfig();
        }
        else {
            this._initLevels();
        }
        this._createController();
        // TODO: Only re-render when necessary
        let render = () => {
            this._render();
            requestAnimationFrame(render);
        };
        window.requestAnimationFrame(render);
        if (this._debug || !document.cookie.includes('isWelcome: true')) {
            this._popUp('Welcome', WELCOME);
            document.cookie = 'isWelcome: true';
        }
    }
    /**
     * Resets player scores and the active player
     */
    _resetPlayerScores() {
        for (let player of this._players) {
            player.resetScore();
        }
        Main.currentPlayer = this._players[0];
        ScoreBoard.update(this._players);
        Main.currentPlayer.move();
    }
    /**
     * Generates a new level
     */
    _generate(generator, size) {
        Main.graphView = generator.generate(size);
        let distributor;
        if (this._negativeWeight) {
            distributor = new RandomNegativeDistributor([-10, 10]);
        }
        else {
            distributor = new RandomDistributor([1, 10]);
        }
        distributor.distribute(Main.graphView);
    }
    /**
     * Sets players and resets scores.
     */
    _setPlayers(players) {
        this._players = players;
        this._resetPlayerScores();
    }
    /**
     * Sets the active level
     * @param id Index of the level
     */
    _setLevel(id) {
        let level = this._levels[id];
        this._generate(level.generator, level.size);
        this._setPlayers([
            // Do we really need to override the player?
            new PlayerHuman(() => {
                this._onPlayerFinishedMove();
            }),
            level.opponent,
        ]);
        let domLevelName = document.querySelector('#level-name');
        domLevelName.innerText = (id + 1).toString();
    }
    /**
     *
     */
    _initLevels() {
        let fnOnPlayerFinishedMove = () => {
            this._onPlayerFinishedMove();
        };
        this._levels.push(new Level(new PlayerBotRandom(fnOnPlayerFinishedMove), new GeneratorLine()));
        this._levels.push(new Level(new PlayerBotGreedy(fnOnPlayerFinishedMove), new GeneratorGrid()));
        this._levels.push(new Level(new PlayerBotHighestWeight(fnOnPlayerFinishedMove), new GeneratorTree()));
        this._levels.push(new Level(new PlayerBotGreedy(fnOnPlayerFinishedMove), new GeneratorRTree()));
        for (let level of this._levels) {
            let sizeGenerator = level.generator.getSizeGenerator();
            let sizeSteps = Math.floor(sizeGenerator.length() * (0.2 + Math.random() * 0.8));
            for (let i = 0; i < sizeSteps; ++i) {
                sizeGenerator.next();
            }
            level.size = sizeGenerator.next();
        }
        this._setLevel(0);
    }
    /**
     * Generates a new graph and players based on the config UI.
     */
    _applyDebugConfig() {
        let layout = document.querySelector('#config #layout');
        let size = document.querySelector('#config #sizeSlider');
        let bot = document.querySelector('#config #bot');
        let negativeWeight = document.querySelector('#config #negativeWeight');
        let generator = GENERATOR_MODES[layout.value].generator();
        if (parseInt(negativeWeight.value)) {
            this._negativeWeight = true;
        }
        else {
            this._negativeWeight = false;
        }
        this._generate(generator, parseInt(size.value));
        let plBot = BOT_MODES[bot.value].bot();
        plBot.setOnMoveComplete(() => { this._onPlayerFinishedMove(); });
        this._setPlayers([
            new PlayerHuman(() => { this._onPlayerFinishedMove(); }),
            plBot
        ]);
    }
    /**
     * Prepares the UI, based on whether or not debugging is enabled.
     */
    _prepareUI() {
        let interactive = document.querySelector('#interactive');
        if (this._debug) {
            let template = document.querySelector('#t-config');
            let domConfig = document.importNode(template, true);
            interactive.insertBefore(domConfig.content, interactive.firstChild);
        }
        else {
            let template = document.querySelector('#t-level-info');
            let domLevelInfo = document.importNode(template, true);
            interactive.insertBefore(domLevelInfo.content, interactive.firstChild);
        }
    }
    /**
     * Display a pop-up window. Interaction with other elements is blocked.
     * There may only be one pop-up at a time!
     * @param title Title text
     * @param body Body text
     */
    _popUp(title, body) {
        let blockOut = document.querySelector('#blockout');
        let popUp = document.querySelector('.popup');
        let elTitle = popUp.querySelector('.title p');
        let elBody = popUp.querySelector('.body p');
        let elOk = popUp.querySelector('#ok');
        if (popUp.style.visibility === 'visible') {
            console.warn(`Duplicate pop-up`);
            console.warn(elTitle.innerText);
            console.warn(`will be replaced with ${title}`);
        }
        let oldBlockOutState = blockOut.style.visibility;
        blockOut.style.visibility = 'visible';
        popUp.style.visibility = 'visible';
        elTitle.innerText = title;
        elBody.innerText = body;
        elOk.onclick = () => {
            blockOut.style.visibility = oldBlockOutState;
            popUp.style.visibility = 'hidden';
        };
    }
    /**
     * Populates the configuration UI
     */
    _populateConfig() {
        let layout = document.querySelector('#config #layout');
        for (let generatorMode of Object.keys(GENERATOR_MODES)) {
            let option = document.createElement('option');
            option.value = generatorMode;
            option.innerText = GENERATOR_MODES[generatorMode].displayName;
            layout.appendChild(option);
        }
        let bot = document.querySelector('#config #bot');
        for (let botMode of Object.keys(BOT_MODES)) {
            let option = document.createElement('option');
            option.value = botMode;
            option.innerText = BOT_MODES[botMode].displayName;
            bot.appendChild(option);
        }
    }
    /**
     * Installs configuration callbacks
     */
    _installConfigCallbacks() {
        let apply = document.querySelector('#config #apply');
        apply.onclick = () => {
            this._applyDebugConfig();
        };
    }
    /**
     * Creates input handlers
     */
    _createController() {
        let controller = new MouseController(this._domCanvas);
        this._domCanvas.onclick = (ev) => {
            controller.onClick(ev);
        };
        this._domCanvas.onmousemove = (ev) => {
            controller.onMouseMove(ev);
        };
    }
    /**
     * Renders a new frame
     */
    _render() {
        this._canvas.clear();
        // TODO: Do we really have to do this every frame?
        this._canvas.fitToWindow();
        this._canvas.graphView(Main.graphView);
    }
    /**
     * Fired when a player won a level
     * @param winner The winning player
     */
    _onPlayerWon(winner) {
        /**
         * Time in milliseconds to wait before advancing to the next level.
         */
        const LEVEL_WAIT_TIME = 3000;
        if (this._debug) {
            console.log(`${winner.name} won the round`);
        }
        else {
            console.log(`${winner.name} won the round`);
            if (winner instanceof PlayerHuman) {
                if (++this._currentLevel >= this._levels.length) {
                    // TODO: Display a splash screen or something
                    console.log(`${winner.name} won the game`);
                }
                else {
                    console.log(`loading next level in ${LEVEL_WAIT_TIME / 1000} second(s)`);
                    setTimeout(() => {
                        this._setLevel(this._currentLevel);
                    }, LEVEL_WAIT_TIME);
                }
            }
            else {
                // TODO: Splash screen
                console.log('human lost, resetting');
                this._currentLevel = 0;
                setTimeout(() => {
                    this._setLevel(this._currentLevel);
                }, LEVEL_WAIT_TIME);
            }
        }
    }
    /**
     * Fired when a player finished their move
     */
    _onPlayerFinishedMove() {
        let gameOver = !Main.graphView.graph.vertexes.find((el) => !el.owner);
        if (gameOver) {
            // All vertexes have been captured
            let winner = this._players[0];
            for (let player of this._players) {
                if (player.score > winner.score) {
                    winner = player;
                }
            }
            winner.winner = true;
            this._onPlayerWon(winner);
        }
        else {
            Main.currentPlayer = this._players[(this._players.indexOf(Main.currentPlayer) + 1) % this._players.length];
            Main.currentPlayer.move();
        }
        ScoreBoard.update(this._players);
    }
}
/**
 * Player whose turn it is
 */
Main.currentPlayer = null;
document.addEventListener('DOMContentLoaded', () => {
    new Main();
});
//# sourceMappingURL=index.js.map