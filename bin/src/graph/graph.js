import { Vertex } from "../game/vertex.js";
export class Graph {
    constructor() {
        /**
         * Array of vertexes.
         * Each vertex consists of a unique identifier and some user defined data.
         */
        this.vertexes = [];
        /**
         * Array of edges.
         * Each edge consists of two vertex ids.
         */
        this.edges = [];
    }
    /**
     * @param vertex Vertex or vertex id
     * @return Ids of neighboring vertexes
     */
    neighbors(vertex) {
        if (vertex instanceof Vertex) {
            vertex = vertex.id;
        }
        let result = [];
        for (let edge of this.edges) {
            let neighbor = null;
            if (edge[0] === vertex) {
                neighbor = edge[1];
            }
            else if (edge[1] === vertex) {
                neighbor = edge[0];
            }
            if (neighbor !== null) {
                if (result.indexOf(neighbor) === -1) {
                    result.push(neighbor);
                }
            }
        }
        return result;
    }
}
//# sourceMappingURL=graph.js.map