"use strict";
class Tabs {
    /**
     * Sets min-width to the widest tab-container's width.
     */
    _fixWidth() {
        let max = 0;
        for (let tabContainer of this._tabContainer.children) {
            max = Math.max(max, tabContainer.clientWidth);
        }
        this._tabs.style.minWidth = `${max.toString()}px`;
    }
    /**
     * Searches for the active tab.
     * Sets {@link _activeTabContainer} and {@link _activeTabSelector}
     */
    _findActiveTab() {
        for (let el of this._tabSelector.children) {
            if (el.classList.contains('active')) {
                this._activeTabSelector = el;
                this._activeTabContainer = this._tabContainer.querySelector(`#${el.getAttribute('for')}`);
                break;
            }
        }
    }
    /**
     * Activates
     * @param id The id of the tab to activate
     */
    _activateTab(id) {
        if (this._activeTabContainer && this._activeTabSelector) {
            this._activeTabContainer.style.display = 'none';
            this._activeTabSelector.classList.remove('active');
        }
        else // No tab has been active yet, so no tab has been hidden either.
         {
            for (let tabContainer of this._tabContainer.children) {
                tabContainer.style.display = 'none';
            }
        }
        this._activeTabSelector = this._tabSelector.querySelector(`[for=${id}]`);
        this._activeTabContainer = this._tabContainer.querySelector(`#${id}`);
        this._activeTabSelector.classList.add('active');
        this._activeTabContainer.style.display = 'block';
    }
    /**
     * Installs tab selector callbacks
     */
    _installCallbacks() {
        for (let selector of this._tabSelector.children) {
            selector.onclick = () => {
                this._activateTab(selector.getAttribute('for'));
            };
        }
    }
    /**
     * @param tabs The tabs container
     * If no tab is active yet, the first tab will be activated.
     */
    constructor(tabs) {
        this._tabs = tabs;
        this._tabSelector = this._tabs.querySelector('.tab-selector');
        this._tabContainer = this._tabs.querySelector('.tab-container');
        this._fixWidth();
        this._findActiveTab();
        if (!this._activeTabContainer && this._tabContainer.firstElementChild) {
            this._activateTab(this._tabContainer.firstElementChild.id);
        }
        this._installCallbacks();
    }
}
// This could also run in a DOM observer, but as long as we don't dynamically
// add tabs, there's no need to do so.
document.addEventListener('DOMContentLoaded', () => {
    for (let el of document.querySelectorAll('.tabs')) {
        let tabs = new Tabs(el);
    }
});
//# sourceMappingURL=tabs.js.map