import { Graph } from './graph.js';
export class GraphView {
    /**
     *
     */
    constructor() {
        this.graph = new Graph();
    }
    /**
     * @param pos position at which to search
     * @return Id of vertex at that position or -1 if none
     */
    getVertexAtPosition(pos) {
        for (let vertex of this.graph.vertexes) {
            if (pos[0] > vertex.position[0] - vertex.radius && pos[0] < vertex.position[0] + vertex.radius) {
                if (pos[1] > vertex.position[1] - vertex.radius && pos[1] < vertex.position[1] + vertex.radius) {
                    return vertex.id;
                }
            }
        }
        return -1;
    }
    /**
     * Searches for a vertex by its id
     * @param id Id of the vertex to search for
     * @return The found vertex
     */
    getVertexById(id) {
        for (let vertex of this.graph.vertexes) {
            if (vertex.id === id) {
                return vertex;
            }
        }
        return null;
    }
}
//# sourceMappingURL=graph_view.js.map