import { WeightDistributor } from "./weight_distributor.js";
export class RandomNegativeDistributor extends WeightDistributor {
    constructor(range) {
        super();
        this.range = range;
    }
    distribute(graph_view) {
        let numOfVertexes = graph_view.graph.vertexes.length;
        // A max. of 2/5 are negative vertexes
        let numOfNegVertexes = Math.floor(Math.random() * (numOfVertexes * (2.0 / 5.0))) + 1;
        let vertexIDs = [];
        let randomVertexIds = [];
        for (let i = 0; i < numOfVertexes; ++i) {
            vertexIDs[i] = graph_view.graph.vertexes[i].id;
        }
        //Get numOfNegVertexes vertex ids from vertexes without repetitions
        for (let i = 1; i <= numOfNegVertexes; ++i) {
            let j = Math.floor(Math.random() * (numOfVertexes - i)) + 1;
            randomVertexIds.push(vertexIDs[j]);
            vertexIDs[j] = vertexIDs[numOfVertexes - i];
        }
        //Set weight to negative random number in range
        for (let i = 0; i < randomVertexIds.length; ++i) {
            graph_view.graph.vertexes[randomVertexIds[i]].weight = Math.floor(Math.random() * this.range[0]);
        }
        //Set weight to positive random number in range
        for (let v of graph_view.graph.vertexes) {
            if (v.weight > 0)
                v.weight = Math.floor(Math.random() * this.range[1] + 1);
        }
    }
}
//# sourceMappingURL=distributor_negative_random.js.map