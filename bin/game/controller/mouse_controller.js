import { Main } from '../../index.js';
export class MouseController {
    /**
     * @param canvas Event-receiving canvas
     * @param graphView The graph view displayed in @canvas
     */
    constructor(canvas) {
        /**
         * Node that the mouse hovered over the last time {@link onMouseMove} was
         * called.
         */
        this._oldHoverNode = -1;
        this._canvas = canvas;
    }
    /**
     * Translates event- to local coordinated
     * @param coordinates Window coordinates [x, y]
     * @return Local coordinates [x, y]
     */
    _translateCoordinates(coordinates) {
        if (coordinates instanceof MouseEvent) {
            coordinates = [coordinates.clientX, coordinates.clientY];
        }
        let rect = this._canvas.getBoundingClientRect();
        return [
            (coordinates[0] - rect.left) / rect.width,
            (coordinates[1] - rect.top) / rect.height
        ];
    }
    /**
     * Click event handler
     * @param event mouse event
     */
    onClick(ev) {
        let position = this._translateCoordinates(ev);
        let vertexId = Main.graphView.getVertexAtPosition(position);
        if (vertexId !== -1) {
            let vertex = Main.graphView.getVertexById(vertexId);
            if (Main.currentPlayer) {
                Main.currentPlayer.onVertexClicked(vertex);
            }
        }
    }
    /**
     * Mouse move event handler
     * @param ev Mouse event
     */
    onMouseMove(ev) {
        const VERTEX_HOVER_GROW = 1.1;
        let position = this._translateCoordinates(ev);
        let vertexId = Main.graphView.getVertexAtPosition(position);
        if (this._oldHoverNode !== -1) {
            if (vertexId !== this._oldHoverNode) {
                let vertex = Main.graphView.getVertexById(this._oldHoverNode);
                if (vertex) {
                    // {@link vertex} can be null when the level changed
                    vertex.radius /= VERTEX_HOVER_GROW;
                }
                this._oldHoverNode = -1;
            }
        }
        if (vertexId !== -1) {
            let vertex = Main.graphView.getVertexById(vertexId);
            if (vertex.id !== this._oldHoverNode) {
                if (!vertex.owner) {
                    vertex.radius *= VERTEX_HOVER_GROW;
                    this._oldHoverNode = vertex.id;
                }
            }
        }
    }
}
//# sourceMappingURL=mouse_controller.js.map