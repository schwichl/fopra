import { PlayerBot } from './player_bot.js';
import { Main } from '../../index.js';
/**
 * Wrapper for the current game state
 */
class GameState {
    constructor(graph, bot) {
        this.taken = new Map();
        this.graph = graph;
        for (let v of graph.graph.vertexes) {
            if (v.owner === bot) {
                this.taken.set(v.id, 1);
            }
            else if (v.owner !== null) {
                this.taken.set(v.id, -1);
            }
        }
    }
    //Evaluates the current state of a player
    evaluate(player) {
        let sumOwn = 0;
        let sumOpponent = 0;
        for (let vid of this.taken.keys()) {
            if (this.taken.get(vid) === player) {
                sumOwn += this.graph.getVertexById(vid).weight;
            }
            else if (this.taken.get(vid) === -player) {
                sumOpponent += this.graph.getVertexById(vid).weight;
            }
        }
        return sumOwn - sumOpponent;
    }
    isValidTurn(vertex) {
        return !this.taken.has(vertex);
    }
    getValidTurns() {
        let turns = [];
        for (let v of this.graph.graph.vertexes) {
            if (this.isValidTurn(v.id))
                turns.push(v.id);
        }
        return turns;
    }
    makeTurn(player, vertex_id) {
        let vertex = this.graph.getVertexById(vertex_id);
        let neighbours = this.graph.graph.neighbors(vertex);
        let taken_vertices = [];
        this.taken.set(vertex_id, player);
        taken_vertices.push(vertex_id);
        for (let neighbour of neighbours) {
            if (this.isValidTurn(neighbour)) {
                this.taken.set(neighbour, player);
                taken_vertices.push(neighbour);
            }
        }
        let turn = {
            player: player,
            vertices: taken_vertices
        };
        return turn;
    }
    reverseTurn(turn) {
        for (let vid of turn.vertices) {
            this.taken.delete(vid);
        }
    }
}
export class PlayerBotMinMax extends PlayerBot {
    /**
   * @override
   */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.selectedTurn = -1;
        this.name = 'MinMaxer';
        this.color = 0x30D030FF;
        this.colorLight = 0x80F080FF;
    }
    //Minimax-Algorithmus
    minmax(state, player, depth, maxDepth) {
        let validTurns = state.getValidTurns();
        if (depth === 0 || validTurns.length === 0) {
            return state.evaluate(player);
        }
        let maxValue = -Infinity;
        for (let turn of validTurns) {
            let newTurn = state.makeTurn(player, turn);
            let value = -this.minmax(state, -player, depth - 1, maxDepth);
            state.reverseTurn(newTurn);
            if (value > maxValue) {
                maxValue = value;
                if (depth === maxDepth) {
                    this.selectedTurn = turn;
                }
            }
        }
        return maxValue;
    }
    //Alpha-Beta-Suche(Optimierter Minimax)
    alphabeta(state, player, depth, alpha, beta, maxDepth) {
        let validTurns = state.getValidTurns();
        if (depth === 0 || validTurns.length === 0) {
            return state.evaluate(player);
        }
        let maxValue = alpha;
        for (let turn of validTurns) {
            let newTurn = state.makeTurn(player, turn);
            let value = -this.alphabeta(state, -player, depth - 1, -beta, -maxValue, maxDepth);
            state.reverseTurn(newTurn);
            if (value > maxValue) {
                maxValue = value;
                if (maxValue >= beta) {
                    break;
                }
                if (depth === maxDepth) {
                    this.selectedTurn = turn;
                }
            }
        }
        return maxValue;
    }
    /**
    * @override
    */
    move() {
        this._lastTimeoutHandle = setTimeout(() => {
            this.selectedTurn = -1;
            let maxDepth = 4;
            let state = new GameState(Main.graphView, this);
            //let score = this.minmax(state, 1, maxDepth, maxDepth);
            let score = this.alphabeta(state, 1, maxDepth, -Infinity, Infinity, maxDepth);
            if (this.selectedTurn >= 0) {
                this.capture(Main.graphView.getVertexById(this.selectedTurn));
            }
            //I'm done!
            this._onMoveComplete();
            this._lastTimeoutHandle = -1;
        }, this._THINK_TIME);
    }
}
//# sourceMappingURL=player_bot_minmax.js.map