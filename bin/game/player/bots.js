import { PlayerBotRandom } from "./player_bot_random.js";
import { PlayerBotGreedy } from "./player_bot_greedy.js";
import { PlayerBotHighestWeight } from "./player_bot_highest_weight.js";
import { PlayerBotMinMax } from "./player_bot_minmax.js";
/**
 * @displayName is used for the debugging GUI, not for the player name!
 */
export const BOT_MODES = {
    'random': {
        displayName: 'Random',
        bot: () => new PlayerBotRandom()
    },
    'highestWeight': {
        displayName: 'Highest Weight',
        bot: () => new PlayerBotHighestWeight()
    },
    'greedy': {
        displayName: 'Greedy',
        bot: () => new PlayerBotGreedy()
    },
    'minmaxer': {
        displayName: 'MinMaxer',
        bot: () => new PlayerBotMinMax()
    }
};
//# sourceMappingURL=bots.js.map