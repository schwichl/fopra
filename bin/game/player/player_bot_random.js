import { PlayerBot } from './player_bot.js';
import { Main } from '../../index.js';
export class PlayerBotRandom extends PlayerBot {
    /**
     * @override
     */
    move() {
        let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);
        console.assert(freeVertexes.length > 0);
        this._lastTimeoutHandle = setTimeout(() => {
            this.capture(freeVertexes[Math.floor(Math.random() * freeVertexes.length)]);
            this._onMoveComplete();
            this._lastTimeoutHandle = -1;
        }, this._THINK_TIME);
    }
    /**
     * @override
     */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.name = 'Randy';
        this.color = 0xE04050FF;
        this.colorLight = 0xFFA0A0FF;
    }
}
//# sourceMappingURL=player_bot_random.js.map