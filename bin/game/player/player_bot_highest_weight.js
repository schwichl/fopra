import { PlayerBot } from './player_bot.js';
import { Main } from '../../index.js';
export class PlayerBotHighestWeight extends PlayerBot {
    /**
       * @override
       */
    move() {
        let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);
        console.assert(freeVertexes.length > 0);
        this._lastTimeoutHandle = setTimeout(() => {
            let bestVertex = freeVertexes[0];
            for (let i = 1; i < freeVertexes.length; i++) {
                if (freeVertexes[i].weight > bestVertex.weight) {
                    bestVertex = freeVertexes[i];
                }
            }
            this.capture(bestVertex);
            this._onMoveComplete();
            this._lastTimeoutHandle = -1;
        }, this._THINK_TIME);
    }
    /**
     * @override
     */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.name = 'Grandy';
        this.color = 0x30D030FF;
        this.colorLight = 0x80F080FF;
    }
}
//# sourceMappingURL=player_bot_highest_weight.js.map