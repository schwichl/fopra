import { Player } from './player.js';
export class PlayerHuman extends Player {
    /**
     * @override
     */
    onVertexClicked(vertex) {
        if (!vertex.owner) {
            this.capture(vertex);
            this._onMoveComplete();
        }
    }
    /**
     * @override
     */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.name = 'Player1';
        this.color = 0x5050E0FF;
        this.colorLight = 0xA0A0FFFF;
    }
}
//# sourceMappingURL=player_human.js.map