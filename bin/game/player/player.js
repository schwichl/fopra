import { Main } from '../../index.js';
export class Player {
    /**
     *
     */
    constructor(onMoveComplete) {
        /**
         * Display name
         */
        this.name = '';
        /**
         * Color used to mark captured nodes
         */
        this.color = 0x00000000;
        /**
         * Color used to mark adjacent nodes
         */
        this.colorLight = 0x00000000;
        /**
         * True if the game is over and this player has won
         */
        this.winner = false;
        /**
         * Sum of all vertexes taken by this player
         */
        this.score = 0;
        /**
         * Timeout handle for bots
         */
        this._lastTimeoutHandle = -1;
        this._onMoveCompleteImpl = onMoveComplete;
    }
    /**
     * Called by child classes
     */
    _onMoveComplete() {
        console.assert(!!this._onMoveCompleteImpl);
        this._onMoveCompleteImpl();
    }
    /**
     * Captures a single vertex, ignoring it's neighbors.
     * The call is ignore if @vertex is already taken
     * @param vertex The vertex to capture
     */
    _captureImpl(vertex, light = false) {
        if (vertex.owner !== null) {
            return;
        }
        vertex.owner = this;
        if (light) {
            vertex.color = this.colorLight;
        }
        else {
            vertex.color = this.color;
        }
        this.score += vertex.weight;
    }
    /**
     * Installs or overrides an onMoveComplete callback
     */
    setOnMoveComplete(onMoveComplete) {
        this._onMoveCompleteImpl = onMoveComplete;
    }
    /**
     * Consumes a vertex and all of it's free neighbors.
     * Increases the player's score.
     * @param vertex The vertex to be captured
     */
    capture(vertex) {
        this._captureImpl(vertex);
        for (let neighbor of Main.graphView.graph.neighbors(vertex)) {
            this._captureImpl(Main.graphView.getVertexById(neighbor), true);
        }
    }
    /**
     * Resets score and winner state
     */
    resetScore() {
        this.score = 0;
        this.winner = false;
    }
    /**
     * Clears the timeout
     */
    clearTimeouts() {
        if (this._lastTimeoutHandle > -1) {
            clearTimeout(this._lastTimeoutHandle);
        }
    }
    /**
     * Fired when the previous player finished their move
     */
    move() { }
    /**
     * Gets called when a vertex is clicked and it's this player's turn
     * @param vertex The clicked vertex
     */
    onVertexClicked(vertex) { }
}
//# sourceMappingURL=player.js.map