import { Player } from './player.js';
import { Main } from '../index.js';
export class PlayerBotRandom extends Player {
    /**
     * @override
     */
    move() {
        let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);
        console.assert(freeVertexes.length > 0);
        setTimeout(() => {
            this.capture(freeVertexes[Math.floor(Math.random() * freeVertexes.length)]);
            this._onMoveComplete();
        }, 200);
    }
    /**
     * @override
     */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.name = 'Randy';
        this.color = 0xE04050FF;
        this.colorLight = 0xFFA0A0FF;
    }
}
//# sourceMappingURL=player_bot_random.js.map