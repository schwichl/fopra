export class Vertex {
    constructor() {
        /**
         * Unique id
         */
        this.id = 0;
        /**
         * x, y
         */
        this.position = [0, 0];
        /**
         * Radius
         */
        this.radius = 0.0;
        /**
         * Color
         */
        this.color = 0x00000000;
        /**
         * Weight
         */
        this.weight = 0;
        /**
         * Owner of the vertex. 0 means not taken.
         */
        this.team = 0;
    }
}
//# sourceMappingURL=vertex_data.js.map