export class Vertex {
    constructor() {
        /**
         * Unique id
         */
        this.id = 0;
        /**
         * x, y
         */
        this.position = [0, 0];
        /**
         * Radius
         */
        this.radius = 0.0;
        /**
         * Color
         */
        this.color = 0x000000FF;
        /**
         * Weight
         */
        this.weight = 0;
        /**
         * Owner of the vertex.
         */
        this._owner = null;
    }
    /**
     *
     */
    get owner() {
        return this._owner;
    }
    /**
     *
     */
    set owner(owner) {
        this._owner = owner;
    }
}
//# sourceMappingURL=vertex.js.map