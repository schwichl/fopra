import { GraphGenerator } from './graph_generator.js';
import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { SizeGenerator } from './size_generator.js';
class _SizeGenerator extends SizeGenerator {
    constructor() {
        super(...arguments);
        this._base = 1;
        this._MAX_BASE = 10;
    }
    /**
     * @override
     */
    min() {
        return 1;
    }
    /**
     * @override
     */
    max() {
        return (this._MAX_BASE * this._MAX_BASE);
    }
    /**
     * @override
     */
    length() {
        return this._MAX_BASE;
    }
    /**
     * @override
     */
    next() {
        let next = super.next();
        this._current = (this._base * this._base);
        ++this._base;
        return next;
    }
    /**
     * @override
     */
    reset() {
        super.reset();
        this._base = 1;
    }
}
;
export class GeneratorGrid extends GraphGenerator {
    generate(size) {
        let squareSize = Math.ceil(Math.sqrt(size));
        let spacing = 1 / squareSize * 0.1;
        let nodeRadius = (1 / squareSize) * 0.5;
        let offset = nodeRadius;
        let result = new GraphView();
        for (let i = 0; i < size; ++i) {
            let x = i % squareSize;
            let y = Math.floor(i / squareSize);
            let vertex = new Vertex();
            vertex.weight = 1;
            vertex.id = i;
            vertex.position = [
                offset + x * (nodeRadius * 2),
                offset + y * (nodeRadius * 2)
            ];
            vertex.radius = nodeRadius - spacing;
            // Create edges
            if (x > 0.0) {
                result.graph.edges.push([i, i - 1]);
            }
            if (y > 0.0) {
                result.graph.edges.push([i, i - squareSize]);
            }
            result.graph.vertexes.push(vertex);
        }
        return result;
    }
    /**
     *
     */
    constructor() {
        super(new _SizeGenerator());
    }
}
//# sourceMappingURL=generator_grid.js.map