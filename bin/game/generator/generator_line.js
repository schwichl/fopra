import { GraphGenerator } from './graph_generator.js';
import { GraphView } from '../../graph/graph_view.js';
import { Vertex } from '../vertex.js';
import { SizeGenerator } from './size_generator.js';
class _SizeGenerator extends SizeGenerator {
    /**
     * @override
     */
    min() {
        return 1;
    }
    /**
     * @override
     */
    max() {
        return 32;
    }
    /**
     * @override
     */
    length() {
        return ((this.max() - this.min()) + 1);
    }
    /**
     * @override
     */
    next() {
        let next = super.next();
        ++this._current;
        return next;
    }
}
;
export class GeneratorLine extends GraphGenerator {
    /**
     * @override
     */
    generate(size) {
        const NODE_RADIUS_STEPS = [
            [0, 0.07],
            [10, 0.06],
            [16, 0.05],
            [20, 0.035],
        ];
        // 0.07 is the fallback for anything larger than in
        // {@link NODE_RADIUS_STEPS}
        let nodeRadius = NODE_RADIUS_STEPS[NODE_RADIUS_STEPS.length - 1][1];
        for (let i = 1; i < NODE_RADIUS_STEPS.length; ++i) {
            if (size < NODE_RADIUS_STEPS[i][0]) {
                nodeRadius = NODE_RADIUS_STEPS[i - 1][1];
            }
        }
        /**
         * Number of nodes before the spiral starts overlapping
         */
        const OUTER_RING_CAPACITY = 10;
        let result = new GraphView();
        for (let i = 0; i < size; ++i) {
            let overlappingProgress = (i / OUTER_RING_CAPACITY);
            let overlappingRadialProgress = (overlappingProgress * Math.PI * 2.0);
            let spiralRadius = ((0.5 - nodeRadius) - (i / OUTER_RING_CAPACITY) * (nodeRadius * 3.0));
            let vertex = new Vertex();
            vertex.id = i;
            vertex.weight = 1;
            if (i !== (size - 1)) {
                result.graph.edges.push([i, i + 1]);
            }
            vertex.position = [
                0.5 + Math.sin(overlappingRadialProgress) * spiralRadius,
                0.5 + Math.cos(overlappingRadialProgress) * spiralRadius - nodeRadius
            ];
            vertex.radius = nodeRadius;
            vertex.radius = nodeRadius;
            result.graph.vertexes.push(vertex);
        }
        return result;
    }
    /**
     *
     */
    constructor() {
        super(new _SizeGenerator());
    }
}
//# sourceMappingURL=generator_line.js.map