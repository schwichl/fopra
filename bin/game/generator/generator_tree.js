import { GraphGenerator } from './graph_generator.js';
import { Vertex } from '../vertex.js';
import { GraphView } from '../../graph/graph_view.js';
import { SizeGenerator } from './size_generator.js';
class _SizeGenerator extends SizeGenerator {
    constructor() {
        super(...arguments);
        this._base = 1;
    }
    /**
     * @override
     */
    min() {
        return 1;
    }
    /**
     * @override
     */
    max() {
        return 50;
    }
    /**
     * @override
     */
    length() {
        return 50;
    }
    /**
     * @override
     */
    next() {
        let next = super.next();
        ++this._current;
        return next;
    }
}
;
export class GeneratorTree extends GraphGenerator {
    /**
     *
     */
    constructor() {
        super(new _SizeGenerator());
        /**
         * Y Distance between vertexes
         */
        this.yDistance = 0;
        this.id = 0;
        /**
         * Radius minimum
         */
        this.minRadius = 0.05;
        /**
         * The deepest path
         */
        this.maxDepth = 0;
    }
    /**
     * Recursive function to built left and right subtree
     */
    generateTree(graphView, vertex, width, height, depth) {
        if (height <= 0) {
            return;
        }
        let tempd = ++depth;
        let v1 = new Vertex();
        let v2 = new Vertex();
        v1.id = this.id++;
        v2.id = this.id++;
        let v1_position = vertex.position[0] - width / 4;
        let v2_position = vertex.position[0] + width / 4;
        //Calculate radius for vertexes by their x-position distance
        let radius = ((vertex.position[0] + width / 4) - (vertex.position[0] - width / 4)) / 2;
        v1.position = [v1_position, vertex.position[1] + this.yDistance];
        v2.position = [v2_position, vertex.position[1] + this.yDistance];
        if (height == 1) {
            //If radius outside the display width minimize radius to x-position
            if (v1_position - radius <= 0 || v1_position - radius >= 1) {
                radius = v1_position;
                this.minRadius = radius; //Set smallest radius
            }
            if (depth > this.maxDepth)
                this.maxDepth = depth;
            graphView.graph.vertexes.push(v1);
            graphView.graph.edges.push([vertex.id, v1.id]);
            return;
        }
        else {
            //If radius outside the display minimize radius to x-position
            if (v1_position - radius <= 0 || v1_position - radius >= 1) {
                radius = v1_position;
            }
            else if (v2_position - radius <= 0 || v2_position - radius >= 1) {
                radius = v2_position;
            }
            if (height == 2) {
                if (depth > this.maxDepth)
                    this.maxDepth = depth;
                graphView.graph.vertexes.push(v1, v2);
                graphView.graph.edges.push([vertex.id, v1.id], [vertex.id, v2.id]);
                if (radius < this.minRadius) {
                    this.minRadius = radius; //Set smallest radius
                }
                return;
            }
            else {
                if (radius < this.minRadius) {
                    this.minRadius = radius; //Set smallest radius
                }
                graphView.graph.vertexes.push(v1, v2);
                graphView.graph.edges.push([vertex.id, v1.id], [vertex.id, v2.id]);
                //Set heights of subtrees
                let leftHeight = Math.floor((height - 2) / 2);
                let rightHeight = Math.round((height - 2) / 2);
                this.generateTree(graphView, v1, width / 2, leftHeight, tempd);
                this.generateTree(graphView, v2, width / 2, rightHeight, tempd);
            }
        }
    }
    /**
     * Generate binary tree
     */
    generate(size) {
        this.yDistance = 0.001; //Start height distance between vertexes
        //Set number of vertexes for left and right subtree
        let leftSubTreeSize = Math.floor((size - 3) / 2);
        let rightSubTreeSize = Math.round((size - 3) / 2);
        let leftSubTreedepth = 0;
        let result = new GraphView();
        //First three vertexes have always the same position
        let x = 0.5;
        for (let i = 0; i < 3; ++i) {
            let roots = new Vertex();
            roots.id = this.id++;
            if (i == 0) {
                roots.position = [x, this.yDistance];
            }
            else {
                if (i == 1) {
                    roots.position = [x - 0.25, result.graph.vertexes[0].position[1] + this.yDistance];
                    this.generateTree(result, roots, 0.5, leftSubTreeSize, leftSubTreedepth);
                    leftSubTreedepth = this.maxDepth;
                }
                else {
                    roots.position = [x + 0.25, result.graph.vertexes[0].position[1] + this.yDistance];
                    this.generateTree(result, roots, 0.5, rightSubTreeSize, 0);
                }
                result.graph.edges.push([0, roots.id]);
            }
            result.graph.vertexes.push(roots);
        }
        //Set same radius for all vertexes
        this.minRadius -= 0.0015; //Subtract line size
        for (let _i = 0; _i < result.graph.vertexes.length; ++_i) {
            result.graph.vertexes[_i].radius = this.minRadius;
        }
        //Set max depth of the binary tree + root and roots of subtrees
        if (leftSubTreedepth > this.maxDepth) {
            this.maxDepth = leftSubTreedepth + 2;
        }
        else {
            this.maxDepth += 2;
        }
        //Orientate binary tree height at display height (y=1)
        let yOld = this.yDistance; //Old start position of first vertex
        let yNew = 2 * this.minRadius; //New start position of first vertex
        this.yDistance = (1 - 2 * this.minRadius) / this.maxDepth; //New distance between vertexes
        //Set new distance between vertexes
        for (let _i = 0; _i < this.maxDepth; ++_i) {
            for (let _j = 0; _j < result.graph.vertexes.length; ++_j) {
                let pos = result.graph.vertexes[_j].position;
                if (pos[1] == yOld) {
                    result.graph.vertexes[_j].position = [pos[0], yNew];
                }
            }
            yOld += 0.001; //Old vertex y-positions at 0.001, 0.002, ...
            yNew += this.yDistance;
        }
        return result;
    }
}
//# sourceMappingURL=generator_tree.js.map