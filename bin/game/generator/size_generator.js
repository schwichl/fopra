export class SizeGenerator {
    /**
     * @return Next size
     */
    next() {
        if (this._current > this.max()) {
            console.error(`size generator exceeded maximum value (${this._current} > ${this.max()})`);
            return null;
        }
        return this._current;
    }
    /**
     * Resets the generator to it's minimum size.
     */
    reset() {
        this._current = this.min();
    }
    /**
     *
     */
    constructor() {
        this._current = this.min();
    }
}
//# sourceMappingURL=size_generator.js.map