import { Player } from './player.js';
import { Main } from '../index.js';
export class PlayerBotGreedy extends Player {
    /**
     * @return Sum of the weights of the given vertex and all of its free
     *         neighbors.
     */
    _getFullWeight(vertex) {
        let result = vertex.weight;
        for (let id of Main.graphView.graph.neighbors(vertex)) {
            let neighbor = Main.graphView.getVertexById(id);
            if (!neighbor.owner) {
                result += neighbor.weight;
            }
        }
        return result;
    }
    /**
     * @override
     */
    move() {
        let freeVertexes = Main.graphView.graph.vertexes.filter((el) => !el.owner);
        console.assert(freeVertexes.length > 0);
        setTimeout(() => {
            let bestVertex = null;
            let bestScore = 0;
            for (let vertex of freeVertexes) {
                let fullWeight = this._getFullWeight(vertex);
                if (fullWeight > bestScore) {
                    bestScore = fullWeight;
                    bestVertex = vertex;
                }
            }
            this.capture(bestVertex);
            this._onMoveComplete();
        }, 200);
    }
    /**
     * @override
     */
    constructor(onMoveComplete) {
        super(onMoveComplete);
        this.name = 'Gree';
        this.color = 0x30D030FF;
        this.colorLight = 0x80F080FF;
    }
}
//# sourceMappingURL=player_bot_greedy.js.map