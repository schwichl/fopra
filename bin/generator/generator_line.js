import { Generator } from './generator.js';
import { Graph } from '../graph/graph.js';
export class GeneratorLine extends Generator {
    generate(size) {
        let result = new Graph();
        for (let i = 0; i < size; ++i) {
            result.vertexes.push(i);
            if (i !== (size - 1)) {
                result.edges.push([i, i + 1]);
            }
        }
        return result;
    }
}
//# sourceMappingURL=generator_line.js.map