export class GraphView {
    /**
     * Renders the next frame
     */
    render() {
        this._context.clearRect(0, 0, this._contextDimensions[0], this._contextDimensions[1]);
        let gradient = this._context.createLinearGradient(0, 0, 500, 500);
        gradient.addColorStop(0.0, '#FF0000');
        gradient.addColorStop(1.0, '#00FF00');
        this._context.beginPath();
        this._context.moveTo(10, 10);
        this._context.lineTo(this._contextDimensions[0] - 10, this._contextDimensions[1] - 10);
        this._context.strokeStyle = gradient;
        this._context.stroke();
    }
    /**
     *
     */
    constructor(context) {
        this._context = context;
        this._context.canvas.width = this._context.canvas.clientWidth;
        this._context.canvas.height = this._context.canvas.clientHeight;
        this._contextDimensions = [this._context.canvas.width, this._context.canvas.height];
    }
}
//# sourceMappingURL=graphview.js.map